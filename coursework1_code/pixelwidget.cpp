#include <QtGui>
#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QPainter>
#include <QLabel>
#include <QDesktopWidget>
#include <iostream>
#include <fstream>
#include "pixelwidget.hpp"


void PixelWidget::DefinePixelValues() { //add pixels here; write methods like this for the assignments
  SetPixel(10,30,RGBVal(255,0,0));
}


void PixelWidget::DrawLine(float x_start, float y_start, float x_end, float y_end, RGBVal rgb_start, RGBVal rgb_end) {
  // number of pixels
  float distance = sqrt(pow(x_end-x_start, 2) + pow(y_end-y_start, 2));
  float t = 1/distance;

  for (float i = 0; i <= 1 ; i += t) {
    float x = (1-i)*x_start + i*x_end;
    float y = (1-i)*y_start + i*y_end;

    float r = ((1-i)*rgb_start._red + i*rgb_end._red);
    float g = ((1-i)*rgb_start._green + i*rgb_end._green);
    float b = ((1-i)*rgb_start._blue + i*rgb_end._blue);

    SetPixel(round(x), round(y), RGBVal((r), (g), (b)));
  }
}


void PixelWidget::DrawTriangle(float ax, float ay, float bx, float by, float cx, float cy, RGBVal rgb_a, RGBVal rgb_b, RGBVal rgb_c) {
  for (float s = 0; s <= 1; s += 0.01) {
    for (float t = 0; t <= 1; t += 0.01) {
      float alpha = 1 - t - s + s*t;
      float beta = t - s*t;
      float gamma = s;

      float x = alpha*ax + beta*bx + gamma*cx;
      float y = alpha*ay + beta*by + gamma*cy;

      float r = alpha*rgb_a._red + beta*rgb_b._red + gamma*rgb_c._red;
      float g = alpha*rgb_a._green + beta*rgb_b._green + gamma*rgb_c._green;
      float b = alpha*rgb_a._blue + beta*rgb_b._blue + gamma*rgb_c._blue;

      SetPixel(round(x), round(y), RGBVal(r, g, b));
    }
  }
}


bool PixelWidget::IsInside(float ax, float ay, float bx, float by, float cx, float cy, unsigned int px, unsigned int py) {
  // distance from point to every vertex of the triangle
  float distance[3];
  distance[0] = sqrt(pow(px-ax, 2) + pow(py-ay, 2));
  distance[1] = sqrt(pow(px-bx, 2) + pow(py-by, 2));
  distance[2] = sqrt(pow(px-cx, 2) + pow(py-cy, 2));

  // find shortest distance
  float shortest = distance[0];
  for (float d : distance) {
    if (d < shortest) {
      shortest = d;
    }
  }

  // calculate the normal of each line of the triangle pointing inside the triangle
  float n_abx = -(by-ay);
  float n_aby = (bx-ax);

  float n_acx = (cy-ay);
  float n_acy = -(cx-ax);

  float n_bcx = -(cy-by);
  float n_bcy = (cx-bx);

  // calculate the midpoints of each triangle line
  float mid_abx = (ax+bx)/2;
  float mid_aby = (ay+by)/2;

  float mid_acx = (ax+cx)/2;
  float mid_acy = (ay+cy)/2;

  float mid_bcx = (bx+cx)/2;
  float mid_bcy = (by+cy)/2;

  // if point is closest to vertex A
  if (shortest == distance[0]) {
    // do the half plane test with both lines from A
    float test = (n_abx * (px-mid_abx)) + (n_aby * (py-mid_aby));
    float test2 = (n_acx * (px-mid_acx)) + (n_acy * (py-mid_acy));

    // not inside triangle if not in the same plane as the normals
    if (test < 0 || test2 < 0) {
      return false;
    }

  // if point is closest to vertex B
  } else if (shortest == distance[1]) {

    float test = (n_abx * (px-mid_abx)) + (n_aby * (py-mid_aby));
    float test2 = (n_bcx * (px-mid_bcx)) + (n_bcy * (py-mid_bcy));

    if (test < 0 || test2 < 0) {
      return false;
    }

  // if point is closest to vertex C
  } else if (shortest == distance[2]) {

    float test = (n_acx * (px-mid_acx)) + (n_acy * (py-mid_acy));
    float test2 = (n_bcx * (px-mid_bcx)) + (n_bcy * (py-mid_bcy));

    if (test < 0 || test2 < 0) {
      return false;
    }
  }
  // point is inside triangle
  return true;
}


// -----------------Most code below can remain untouched -------------------------
// ------but look at where DefinePixelValues is called in paintEvent--------------

PixelWidget::PixelWidget(unsigned int n_vertical, unsigned int n_horizontal):
  _n_vertical   (n_vertical),
  _n_horizontal (n_horizontal),
  _vec_rects(0)
{
  // all pixels are initialized to black
     for (unsigned int i_col = 0; i_col < n_vertical; i_col++)
       _vec_rects.push_back(std::vector<RGBVal>(n_horizontal));
}


void PixelWidget::SetPixel(unsigned int i_column, unsigned int i_row, const RGBVal& rgb)
{

  // if the pixel exists, set it
  if (i_column < _n_vertical && i_row < _n_horizontal)
    _vec_rects[i_column][i_row] = rgb;
}


void PixelWidget::paintEvent( QPaintEvent * )
{

  QPainter p( this );
  // no antialiasing, want thin lines between the cell
  p.setRenderHint( QPainter::Antialiasing, false );

  // set window/viewport so that the size fits the screen, within reason
  p.setWindow(QRect(-1.,-1.,_n_vertical+2,_n_horizontal+2));
  int side = qMin(width(), height());
  p.setViewport(0, 0, side, side);

  // black thin boundary around the cells
  QPen pen( Qt::black );
  pen.setWidth(0.);

  // here the pixel values defined by the user are set in the pixel array
  DefinePixelValues();

  // draw a line for assignments 1 and 2
  // DrawLine(10,30, 50,5, RGBVal(0,0,255), RGBVal(255,0,0));

  // draw a triangle for assignment 3
  DrawTriangle(10,30, 50,5, 50,60, RGBVal(255, 0, 255), RGBVal(255, 255, 0), RGBVal(0, 255, 255));

  // output to files
  std::ofstream out("results.txt");   // for barycentric coordinates and IsInside
  std::ofstream ppm("image.ppm");     // PPM image

  // PPM header
  ppm << "P3 70 70 255\n";
  // output every RGB value to ppm file
  for (unsigned int i_row = 0; i_row < _n_horizontal; i_row++) {
    for(unsigned int i_column = 0 ; i_column < _n_vertical; i_column++) {
      ppm << _vec_rects[i_column][i_row]._red << " " << _vec_rects[i_column][i_row]._green << " " << _vec_rects[i_column][i_row]._blue << "\n";
    }
  }
  ppm.close();

  // (x1-x3)*(y2-y3))-((x2-x3)*(y1-y3)
  float determinant = ((10-50)*(5-60))-((50-50)*(30-60));

  for (unsigned int i_column = 0 ; i_column < _n_vertical; i_column++) {
    for(unsigned int i_row = 0; i_row < _n_horizontal; i_row++) {
      QRect rect(i_column,i_row,1,1);
      QColor c = QColor(_vec_rects[i_column][i_row]._red, _vec_rects[i_column][i_row]._green, _vec_rects[i_column][i_row]._blue);

      // fill with color for the pixel
      p.fillRect(rect, QBrush(c));
      p.setPen(pen);
      p.drawRect(rect);

      // calculate barycentric coordinates
      float alpha = (((5-60)*(i_row-50))+((50-50)*(i_column-60)))/determinant;
      float beta = (((60-30)*(i_row-50))+((10-50)*(i_column-60)))/determinant;
      float gamma = 1 - (alpha + beta);

      // check if pixel is inside or outside triangle and write barycentric coordinates to file
      if (IsInside(10,30, 50,5, 50,60, i_row, i_column)) {
        out << alpha << ", " << beta << ", " << gamma << "\tinside\n";

      } else {
        out << alpha << ", " << beta << ", " << gamma << "\toutside\n";
      }
    }
  }
  out.close();
}

#include "LighthouseWindow.h"
#include <iostream>

// constructor / destructor
LighthouseWindow::LighthouseWindow(QWidget *parent)
	: QWidget(parent)
	{ // constructor

	// create menu bar
	menuBar = new QMenuBar(this);

	// create file menu
	fileMenu = menuBar->addMenu("&File");

	// create the action
	actionQuit = new QAction("&Quit", this);

	// leave signals & slots to the controller

	// add the item to the menu
	fileMenu->addAction(actionQuit);

	// create the window layout
	windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);

	// create main widget
	lighthouseWidget = new LighthouseWidget(this);
	windowLayout->addWidget(lighthouseWidget);

	// create slider to control the speed of the boat
	speedSlider = new QSlider(Qt::Horizontal);
    connect(speedSlider, SIGNAL(valueChanged(int)), lighthouseWidget, SLOT(updateBoatSpeed(int)));
    windowLayout->addWidget(speedSlider);

	// timer that changes the angle variable for animation
	ptimer = new QTimer(this);
    connect(ptimer, SIGNAL(timeout()),  lighthouseWidget, SLOT(updateAngle()));
    ptimer->start(0);

	// slider to change the camera view
	viewSlider = new QSlider(Qt::Horizontal);
    connect(viewSlider, SIGNAL(valueChanged(int)), lighthouseWidget, SLOT(updateViewAngle(int)));
		viewSlider->setMinimum(-29);
		viewSlider->setMaximum(-1);
		viewSlider->setValue(-15);
    windowLayout->addWidget(viewSlider);

	// create radio buttons for different coloured fish
	fishButton = new QRadioButton("Blue/purple fish", this);
		connect(fishButton, SIGNAL(pressed()), lighthouseWidget, SLOT(updateBlueFish()));
		windowLayout->addWidget(fishButton);

	fishButton2 = new QRadioButton("Yellow/green fish", this);
		connect(fishButton2, SIGNAL(pressed()), lighthouseWidget, SLOT(updateYellowFish()));
		windowLayout->addWidget(fishButton2);

	fishButton3 = new QRadioButton("Red/orange fish", this);
		connect(fishButton3, SIGNAL(pressed()), lighthouseWidget, SLOT(updateRedFish()));
		windowLayout->addWidget(fishButton3);

	fishButton4 = new QRadioButton("Rainbow fish", this);
		connect(fishButton4, SIGNAL(pressed()), lighthouseWidget, SLOT(updateRGBFish()));
		windowLayout->addWidget(fishButton4);

	fishButton5 = new QRadioButton("Rainbow 2 fish", this);
		connect(fishButton5, SIGNAL(pressed()), lighthouseWidget, SLOT(updateCMYFish()));
		windowLayout->addWidget(fishButton5);
	} // constructor

LighthouseWindow::~LighthouseWindow()
	{ // destructor
	delete fishButton;
	delete fishButton2;
	delete fishButton3;
	delete fishButton4;
	delete fishButton5;
	delete ptimer;
	delete speedSlider;
	delete viewSlider;
	delete lighthouseWidget;
	delete windowLayout;
	delete actionQuit;
	delete fileMenu;
	delete menuBar;
	} // destructor

// resets all the interface elements
void LighthouseWindow::ResetInterface()
	{ // ResetInterface()
	// now force refresh
	lighthouseWidget->update();
	update();
	} // ResetInterface()

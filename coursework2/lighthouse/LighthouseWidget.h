#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <QGLWidget>
#include <QImage>
#include <GL/glut.h>
#include "Image.h"

class LighthouseWidget: public QGLWidget {
	Q_OBJECT

	public:

	LighthouseWidget(QWidget *parent);
	~LighthouseWidget();

	public slots:
	void updateAngle();
	void updateBoatSpeed(int i);
	void updateViewAngle(int i);
	void updateRedFish();
	void updateYellowFish();
	void updateBlueFish();
	void updateRGBFish();
	void updateCMYFish();

	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	// called every time the widget needs painting
	void paintGL();

	private:
		// lighthouse
		void base();
		void level1();
		void level2();
		void level3();
		void level4();
		void level5();
		void light();
		void top();
		void ball();
		void lighthouse();

		// boat
		void boatBase();
		void boatSide();
		void pole();
		void flag();
		void sail();
		void boat();

		// house
		void housePentagon();
		void houseRectangle();
		void roof();
		void house();

		void sea();
		void grass();
		void soil();
		void island();
		void earth();
		void fish();
		void schoolOfFish();

		GLUquadricObj* pbase;
		GLUquadricObj* plevel1;
		GLUquadricObj* plevel2;
		GLUquadricObj* plevel3;
		GLUquadricObj* plevel4;
		GLUquadricObj* plevel5;
		GLUquadricObj* plight;
		GLUquadricObj* ptop;
		GLUquadricObj* pball;
		GLUquadricObj* ppole;


		int angle;
		int boatSpeed;
		int viewAngle;

		Image _image;
		Image _image2;
		Image _image3;
		QImage* p_qimage;

}; // class GLPolygonWidget

#endif

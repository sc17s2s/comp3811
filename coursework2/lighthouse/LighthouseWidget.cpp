#include <GL/glu.h>
#include <QGLWidget>
#include <QSlider>
#include <cmath>
#include <iostream>
#include "LighthouseWidget.h"

// variables used to texture the earth
const int NR_PHI   = 20;
const int NR_THETA = 20;

// flag rotation counter and direction
int counter = 0;
bool ccw = NULL;

// variables to check whether a certain fish colour has been set
bool redFish = NULL;
bool yellowFish = NULL;
bool blueFish = NULL;
bool rgbFish = NULL;
bool cmyFish = NULL;

// Setting up material properties
typedef struct materialStruct {
  GLfloat ambient[4];
  GLfloat diffuse[4];
  GLfloat specular[4];
  GLfloat shininess;
} materialStruct;

static materialStruct whiteShinyMaterials = {
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  100.0
};

static materialStruct redPlasticMaterials = {
  {0.3, 0.0, 0.0, 1.0},
  {0.6, 0.0, 0.0, 1.0},
  {0.8, 0.6, 0.6, 1.0},
  32.0
};

static materialStruct goldMaterials = {
  { 0.24725f, 0.1995f, 0.0745f, 1.0f },
  { 0.75164f, 0.60648f, 0.22648f, 1.0f },
  { 0.628281f, 0.555802f, 0.366065f, 1.0f },
  51.2f
};

// constructor
LighthouseWidget::LighthouseWidget(QWidget *parent)
  : QGLWidget(parent),
    angle(0),
    boatSpeed(0),
    viewAngle(0),
    _image("./Moi.ppm"),
    _image2("./sea.jpg"),
    _image3("./earth.ppm")
	{ // constructor
    pbase      = gluNewQuadric();
	  plevel1    = gluNewQuadric();
    plevel2    = gluNewQuadric();
    plevel3    = gluNewQuadric();
    plevel4    = gluNewQuadric();
    plevel5    = gluNewQuadric();
    plight     = gluNewQuadric();
    ptop       = gluNewQuadric();
    pball      = gluNewQuadric();
    ppole      = gluNewQuadric();
} // constructor

// called when OpenGL context is set up
void LighthouseWidget::initializeGL()
{ // initializeGL()
	// set the widget background colour
	glClearColor(0.4, 0.7, 1., 0.0);
} // initializeGL()

LighthouseWidget::~LighthouseWidget()
{
  gluDeleteQuadric(pbase);
  gluDeleteQuadric(plevel1);
  gluDeleteQuadric(plevel2);
  gluDeleteQuadric(plevel3);
  gluDeleteQuadric(plevel4);
  gluDeleteQuadric(plevel5);
  gluDeleteQuadric(plight);
  gluDeleteQuadric(ptop);
  gluDeleteQuadric(pball);
  gluDeleteQuadric(ppole);
}

// update angle for animation and fluttering of flag
void LighthouseWidget::updateAngle() {
  angle += 1.;

  // set to move flag counter-clockwise if less than 10 or clockwise if greater
  if (counter < 10) {
    counter += 1;
    ccw = true;
  } else if (counter > 9) {
    counter += 1;
    ccw = false;
  }
  this->repaint();
}

// changes the boat speed to the value set with the slider
void LighthouseWidget::updateBoatSpeed(int i) {
  boatSpeed = i;
  this->repaint();
}

// changes the camera view to the value set with the slider
void LighthouseWidget::updateViewAngle(int i) {
  viewAngle = i;
  this->repaint();
}

// for each update function, set the respective fish colour to true and all others to false
void LighthouseWidget::updateBlueFish() {
  blueFish = true;
  redFish = false;
  yellowFish = false;
  rgbFish = false;
  cmyFish = false;
  this->repaint();
}

void LighthouseWidget::updateYellowFish() {
  yellowFish = true;
  redFish = false;
  blueFish = false;
  rgbFish = false;
  cmyFish = false;
  this->repaint();
}

void LighthouseWidget::updateRedFish() {
  redFish = true;
  blueFish = false;
  yellowFish = false;
  rgbFish = false;
  cmyFish = false;
  this->repaint();
}

void LighthouseWidget::updateRGBFish() {
  rgbFish = true;
  redFish = false;
  yellowFish = false;
  blueFish = false;
  cmyFish = false;
  this->repaint();
}

void LighthouseWidget::updateCMYFish() {
  cmyFish = true;
  redFish = false;
  yellowFish = false;
  blueFish = false;
  rgbFish = false;
  this->repaint();
}

// called every time the widget is resized
void LighthouseWidget::resizeGL(int w, int h)
	{ // resizeGL()
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);
	GLfloat light_pos[] = {-4.,3,10., 0.};

	glEnable(GL_LIGHTING); // enable lighting in general
        glEnable(GL_LIGHT0);   // each light source must also be enabled

  glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
       	glLightf (GL_LIGHT0, GL_SPOT_CUTOFF,150.);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-10.0, 10.0, -10.0, 10.0, -40.0, 15.0);

	} // resizeGL()

// different levels of lighthouse
void LighthouseWidget::base() {
  gluCylinder(pbase,1.3,1.5,1,20,20);
}

void LighthouseWidget::level1() {
  gluCylinder(plevel1,1.1,1.3,2,40,20);
}

void LighthouseWidget::level2() {
  gluCylinder(plevel2,1.0,1.1,2,40,20);
}

void LighthouseWidget::level3() {
  gluCylinder(plevel3,0.8,1.0,2,40,20);
}

void LighthouseWidget::level4() {
  gluCylinder(plevel4,1.0,0.8,0.5,40,20);
}

void LighthouseWidget::level5() {
  gluCylinder(plevel5,0.7,0.6,0.3,40,20);
}

// spherical light of lighthouse
void LighthouseWidget::light() {
  gluSphere(plight,0.6,20,20);
}

// cone shaped roof of lighthouse
void LighthouseWidget::top() {
  gluCylinder(ptop,0.1,0.7,0.6,40,20);
}

// sphere on top of lighthouse
void LighthouseWidget::ball() {
  gluSphere(pball,0.2,20,20);
}

// assemble lighthouse
void LighthouseWidget::lighthouse() {
  glPushMatrix();
    // move lighthouse on top of the island
    glTranslatef(0.,2.,0.);

    // white objects
    materialStruct* p_front = &whiteShinyMaterials;

    glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
    glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

    // create the base
    glPushMatrix();
    glRotatef(90.,1.,0.,0.);
    this->base();
    glPopMatrix();

    // create level 2
    glPushMatrix();
    glTranslatef(0.,4.,0.);
    glRotatef(90.,1.,0.,0.);
    this->level2();
    glPopMatrix();

    // create level 4
    glPushMatrix();
    glTranslatef(0.,6.5,0.);
    glRotatef(90.,1.,0.,0.);
    this->level4();
    glPopMatrix();

    // level 5
    glPushMatrix();
    glTranslatef(0.,7.4,0.);
    glRotatef(90.,1.,0.,0.);
    this->level5();
    glPopMatrix();

    // create the bulb
    glPushMatrix();
    glTranslatef(0., 6.7, 0.);
    this->light();
    glPopMatrix();

    // red objects
    p_front = &redPlasticMaterials;

    glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
    glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

    // level 1
    glPushMatrix();
    glTranslatef(0.,2.,0.);
    glRotatef(90.,1.,0.,0.);
    this->level1();
    glPopMatrix();

    // level 3
    glPushMatrix();
    glTranslatef(0.,6.,0.);
    glRotatef(90.,1.,0.,0.);
    this->level3();
    glPopMatrix();

    // top cone
    glPushMatrix();
    glTranslatef(0.,8,0.);
    glRotatef(90.,1.,0.,0.);
    this->top();
    glPopMatrix();

    // ball
    glPushMatrix();
    glTranslatef(0.,8.1,0.);
    this->ball();
    glPopMatrix();
  glPopMatrix();
}

// mast of boat
void LighthouseWidget::pole() {
  gluCylinder(ppole,0.1,0.1,6,8,8);
}

// bottom of the boat
void LighthouseWidget::boatBase() {
  glBegin(GL_POLYGON);
    glVertex3f( 0.0, 0.0, 1.0);
    glVertex3f(-1.0, 0.0,-2.0);
    glVertex3f(-2.0, 0.0, 1.0);
    glVertex3f(-1.0, 0.0, 4.0);
  glEnd();
}

// quadrilateral side of boat
void LighthouseWidget::boatSide() {
  glBegin(GL_POLYGON);
    glVertex3f( 0.0, 0.0,  1.0);
    glVertex3f(-1.0, 0.0, -2.0);
    glVertex3f(-1.0, 1.0, -2.0);
    glVertex3f( 0.5, 0.7,  1.0);
  glEnd();
}

// fluttering textured flag
void LighthouseWidget::flag() {
  // use Moi texture
  glEnable(GL_TEXTURE_2D);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _image.Width(), _image.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _image.imageField());

  // reset rotation counter to switch between clockwise and counter-clockwise
  if (counter == 20) {
    counter = 0;
  }
  // rotate flag either clockwise or counter-clockwise
  if (ccw) {
    glRotatef(counter/2, 0, 0, 1);
  } else if (!ccw) {
    glRotatef(-(counter/2), 0, 0, 1);
  }

  glTranslatef(0.,-1.,3.);
  glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 1.0);
    glVertex3f( 0.0, -1.0,  3.0);
    glTexCoord2f(0.0, 0.0);
    glVertex3f( 0.0, -1.0,  1.0);
    glTexCoord2f(1.0, 0.0);
    glVertex3f( 0.0,  1.0,  1.0);
    glTexCoord2f(1.0, 1.0);
    glVertex3f( 0.0,  1.0,  3.0);
  glEnd();
  glDisable(GL_TEXTURE_2D);
}

// multicoloured sail
void LighthouseWidget::sail() {
  glEnable(GL_COLOR_MATERIAL);
  glBegin(GL_POLYGON);
    glColor4f(1.0, 1.0, 0.0, 1);    // yellow
    glVertex3f( 0.0, 1.1,  3.0);
    glColor4f(0.0, 1.0, 1.0, 1);    // cyan
    glVertex3f( 0.0, 1.1, -2.0);
    glColor4f(1.0, 0.0, 1.0, 1);    // magenta
    glVertex3f( 0.0, 3.5, -1.5);
  glEnd();
  glDisable(GL_COLOR_MATERIAL);
}

// assemble all parts of the boat
void LighthouseWidget::boat() {
  // material for the front of polygon
  materialStruct* p_front = &goldMaterials;

  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

  // material for back of polygon
  materialStruct* p_back = &goldMaterials;

  glMaterialfv(GL_BACK, GL_AMBIENT,    p_back->ambient);
  glMaterialfv(GL_BACK, GL_DIFFUSE,    p_back->diffuse);
  glMaterialfv(GL_BACK, GL_SPECULAR,   p_back->specular);
  glMaterialf(GL_BACK, GL_SHININESS,   p_back->shininess);

  // set both sides of polygon to have light and be seen
  glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

  glPushMatrix();
    // rotate boat around lighthouse
    glRotatef((double)angle/10 + ((double)boatSpeed*(double)angle/20),0.,1.,0.);
    glTranslatef(8.,0.1,0.);
    // create base of boat
    glNormal3f(0, 1, 0);
    this->boatBase();
    // create the sides of the boat
    glPushMatrix();
      // 1st side
      glNormal3f(0.785, -0.561, -0.262);
      this->boatSide();
      glPushMatrix();
        // 2nd side
        glTranslatef(0.,0.,2.);
        glRotatef(180.,0.,1.,0.);
        glScalef(-1,1,1);
        glNormal3f(-0.785, 0.561, 0.262);
        this->boatSide();
      glPopMatrix();
      glPushMatrix();
        // 3rd side
        glTranslatef(-2.,0.,0.);
        glScalef(-1,1,1);
        glNormal3f(-0.785, 0.561, 0.262);
        this->boatSide();
      glPopMatrix();
      glPushMatrix();
        // 4th side
        glTranslatef(-2.,0.,2.);
        glRotatef(180.,1.,0.,0.);
        glRotatef(180.,0.,0.,1.);
        glNormal3f(0.785, -0.561, -0.262);
        this->boatSide();
      glPopMatrix();
    glPopMatrix();

    // change material to white shiny
  	p_front = &whiteShinyMaterials;

    glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  	glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  	glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  	glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

    p_back = &whiteShinyMaterials;

    glMaterialfv(GL_BACK, GL_AMBIENT,    p_back->ambient);
    glMaterialfv(GL_BACK, GL_DIFFUSE,    p_back->diffuse);
    glMaterialfv(GL_BACK, GL_SPECULAR,   p_back->specular);
    glMaterialf(GL_BACK, GL_SHININESS,   p_back->shininess);

    // pole, sail and flag
    glPushMatrix();
      glTranslatef(-1.,0.,2.);
      glRotatef(-90.,1.,0.,0.);
      this->pole();
      glPushMatrix();
        glNormal3f(1, 0, 0);
        this->flag();
      glPopMatrix();
      glPushMatrix();
        glTranslatef(0.,-1.,3.);
        glNormal3f(1, 0, 0);
        this->sail();
      glPopMatrix();
  	glPopMatrix();
  glPopMatrix();

  // turn light on both sides of polygon off
  glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
}

// pentagonal wall of house
void LighthouseWidget::housePentagon() {
  glColor4f(1.0, 0.99, 0.82, 1);    // cream
  glBegin(GL_POLYGON);
    glVertex3f( 0.0, 0.0,  0.0);
    glVertex3f( 0.0, 0.0,  1.5);
    glVertex3f( 0.0, 1.0,  1.5);
    glVertex3f( 0.0, 1.5,  0.75);
    glVertex3f( 0.0, 1.0,  0.0);
  glEnd();
}

// rectangular wall of house
void LighthouseWidget::houseRectangle() {
  glColor4f(1.0, 0.99, 0.82, 1);    // cream
  glBegin(GL_POLYGON);
    glVertex3f( 1.5, 0.0,  0.0);
    glVertex3f( 0.0, 0.0,  0.0);
    glVertex3f( 0.0, 1.0,  0.0);
    glVertex3f( 1.5, 1.0,  0.0);
  glEnd();
}

// roof panel of house
void LighthouseWidget::roof() {
  glColor4f(0.71, 0.2, 0.2, 1);
  glNormal3f(0, 0.832, -0.555);     // terracotta red
  glBegin(GL_POLYGON);
    glVertex3f( 1.5, 1.0,  0.0);
    glVertex3f( 0.0, 1.0,  0.0);
    glVertex3f( 0.0, 1.5,  0.75);
    glVertex3f( 1.5, 1.5,  0.75);
  glEnd();
}

// assemble house
void LighthouseWidget::house() {
  // hierarchical from the first pentagon wall
  glPushMatrix();
    glNormal3f(-1.0, 0.0, 0.0);
    this->housePentagon();
    glPushMatrix();
      glTranslatef(1.5, 0, 0);
      glNormal3f(1.0, 0.0, 0.0);
      this->housePentagon();
    glPopMatrix();
    glPushMatrix();
      glNormal3f(0.0, 0.0, -1.0);
      this->houseRectangle();
    glPopMatrix();
    glPushMatrix();
      glTranslatef(0, 0, 1.5);
      glNormal3f(0.0, 0.0, 1.0);
      this->houseRectangle();
    glPopMatrix();
    glPushMatrix();
      this->roof();
      glScalef(1, 1, -1);
      glTranslatef(0, 0, -1.5);
      this->roof();
    glPopMatrix();
  glPopMatrix();
}

// coloured and textured sea
void LighthouseWidget::sea() {
  // use a texture
  glEnable(GL_TEXTURE_2D);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _image2.Width(), _image2.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _image2.imageField());
  glBegin(GL_POLYGON);
  glNormal3f(0.0, 1.0, 0.0);
    // cyan colour
    glColor4f(0.0, 1.0, 1.0, 1);
    glTexCoord2f(0.0, 0.0);
    glVertex3f( 50.0, 0.0, -10.0);
    glTexCoord2f(1.0, 0.0);
    glVertex3f( 50.0, 0.0,  30.0);
    // darker sea blue colour
    glColor4f(0.0, 0.47, 1, 1);
    glTexCoord2f(1.0, 1.0);
    glVertex3f(-10.0, 0.0,  30.0);
    glTexCoord2f(0.0, 1.0);
    glVertex3f(-10.0, 0.0, -10.0);
  glEnd();
  glDisable(GL_TEXTURE_2D);
}

// island grass
void LighthouseWidget::grass() {
  glBegin(GL_POLYGON);
  glColor4f(0.0, 0.48, 0.047, 1);   // grass green
  glNormal3f(0.0, 1.0, 0.0);
    glVertex3f( 1.5, 1.0, 1.5);
    glVertex3f( 2.0, 1.0, 0.0);
    glVertex3f( 2.0, 1.0,-1.5);
    glVertex3f( 2.0, 1.0,-3.0);
    glVertex3f( 1.0, 1.0,-4.0);
    glVertex3f( 0.0, 1.0,-4.5);
    glVertex3f(-1.0, 1.0,-4.5);
    glVertex3f(-2.0, 1.0,-3.5);
    glVertex3f(-2.5, 1.0,-2.5);
    glVertex3f(-2.5, 1.0,-1.5);
    glVertex3f(-2.5, 1.0,-0.5);
    glVertex3f(-2.0, 1.0, 1.0);
    glVertex3f(-1.0, 1.0, 1.5);
    glVertex3f( 0.0, 1.0, 2.0);
  glEnd();
}

// island cliffs and beaches
void LighthouseWidget::soil() {
  glBegin(GL_POLYGON);
  glNormal3f(0.89, 0.0, 0.45);
    glColor4f(0.88, 0.75, 0.57, 1);   // sand
    glVertex3f(2.0, 0.0, 2.0);
    glVertex3f(3.0, 0.0, 0.0);
    glColor4f(0.47, 0.27, 0.21, 1);   // brown
    glVertex3f(2.0, 1.0, 0.0);
    glVertex3f(1.5, 1.0, 1.5);
  glEnd();
  glBegin(GL_POLYGON);
  glNormal3f(0.96, 0, 0.29);
    glColor4f(0.88, 0.75, 0.57, 1);
    glVertex3f(3.0, 0.0, 0.0);
    glVertex3f(3.3, 0.0,-1.0);
    glColor4f(0.47, 0.27, 0.21, 1);
    glVertex3f(2.0, 1.0,-1.5);
    glVertex3f(2.0, 1.0, 0.0);
  glEnd();
  glBegin(GL_POLYGON);
  glNormal3f(0.99, 0, 0.13);
    glColor4f(0.88, 0.75, 0.57, 1);
    glVertex3f(3.3, 0.0,-1.0);
    glVertex3f(3.5, 0.0,-2.5);
    glColor4f(0.47, 0.27, 0.21, 1);
    glVertex3f(2.0, 1.0,-3.0);
    glVertex3f(2.0, 1.0,-1.5);
  glEnd();
  glBegin(GL_POLYGON);
  glNormal3f(0.71, 0, -0.71);
    glColor4f(0.88, 0.75, 0.57, 1);
    glVertex3f(3.5, 0.0,-2.5);
    glVertex3f(2.0, 0.0,-4.0);
    glColor4f(0.47, 0.27, 0.21, 1);
    glVertex3f(1.0, 1.0,-4.0);
    glVertex3f(2.0, 1.0,-3.0);
  glEnd();
  glBegin(GL_POLYGON);
  glNormal3f(0.45, 0, -0.89);
    glColor4f(0.88, 0.75, 0.57, 1);
    glVertex3f(2.0, 0.0,-4.0);
    glVertex3f(0.0, 0.0,-5.0);
    glColor4f(0.47, 0.27, 0.21, 1);
    glVertex3f(0.0, 1.0,-4.5);
    glVertex3f(1.0, 1.0,-4.0);
  glEnd();
  glBegin(GL_POLYGON);
  glNormal3f(-0.24, 0, -0.97);
    glColor4f(0.88, 0.75, 0.57, 1);
    glVertex3f( 0.0, 0.0,-5.0);
    glVertex3f(-2.0, 0.0,-4.5);
    glColor4f(0.47, 0.27, 0.21, 1);
    glVertex3f(-1.0, 1.0,-4.5);
    glVertex3f( 0.0, 1.0,-4.5);
  glEnd();
  glBegin(GL_POLYGON);
  glNormal3f(-0.71, 0, -0.71);
    glVertex3f(-2.0, 0.0,-4.5);
    glVertex3f(-2.5, 0.0,-4.0);
    glVertex3f(-2.0, 1.0,-3.5);
    glVertex3f(-1.0, 1.0,-4.5);
  glEnd();
  glBegin(GL_POLYGON);
  glNormal3f(-0.95, 0, -0.32);
    glVertex3f(-2.5, 0.0,-4.0);
    glVertex3f(-3.0, 0.0,-2.5);
    glVertex3f(-2.5, 1.0,-2.5);
    glVertex3f(-2.0, 1.0,-3.5);
  glEnd();
  glBegin(GL_POLYGON);
  glNormal3f(-1, 0, 0);
    glVertex3f(-3.0, 0.0,-2.5);
    glVertex3f(-3.0, 0.0,-0.5);
    glVertex3f(-2.5, 1.0,-0.5);
    glVertex3f(-2.5, 1.0,-2.5);
  glEnd();
  glBegin(GL_POLYGON);
  glNormal3f(-0.95, 0, 0.32);
    glVertex3f(-3.0, 0.0,-0.5);
    glVertex3f(-2.5, 0.0, 1.0);
    glVertex3f(-2.0, 1.0, 1.0);
    glVertex3f(-2.5, 1.0,-0.5);
  glEnd();
  glBegin(GL_POLYGON);
  glNormal3f(-0.71, 0, 0.71);
    glVertex3f(-2.5, 0.0, 1.0);
    glVertex3f(-1.5, 0.0, 2.0);
    glVertex3f(-1.0, 1.0, 1.5);
    glVertex3f(-2.0, 1.0, 1.0);
  glEnd();
  glBegin(GL_POLYGON);
  glNormal3f(-0.24, 0, 0.97);
    glVertex3f(-1.5, 0.0, 2.0);
    glVertex3f( 0.5, 0.0, 2.5);
    glVertex3f( 0.0, 1.0, 2.0);
    glVertex3f(-1.0, 1.0, 1.5);
  glEnd();
  glBegin(GL_POLYGON);
  glNormal3f(0.32, 0, 0.95);
    glColor4f(0.88, 0.75, 0.57, 1);
    glVertex3f( 0.5, 0.0, 2.5);
    glVertex3f( 2.0, 0.0, 2.0);
    glColor4f(0.47, 0.27, 0.21, 1);
    glVertex3f( 1.5, 1.0, 1.5);
    glVertex3f( 0.0, 1.0, 2.0);
  glEnd();
}

// join parts of island including the house and lighthouse
void LighthouseWidget::island() {
  glPushMatrix();
    // island rotation angle
    glRotatef((double)-angle/2,0.,1.,0.);

    glEnable(GL_COLOR_MATERIAL);
    glPushMatrix();
      this->grass();
      this->soil();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-0.5,1.,-3.);
    this->house();
    glPopMatrix();
    glDisable(GL_COLOR_MATERIAL);

    glPushMatrix();
      this->lighthouse();
    glPopMatrix();
  glPopMatrix();
}

// planet earth
void LighthouseWidget::earth() {
  // orbit
  glRotatef((double)angle/2,0.,0.,1.);

  glEnable(GL_TEXTURE_2D);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _image3.Width(), _image3.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _image3.imageField());
  for(int longitude = 0; longitude < NR_PHI; longitude++){
    for(int latitude = 0; latitude < NR_THETA; latitude++){
      float d_phi   = 2*M_PI/NR_PHI;
      float d_theta = M_PI/NR_THETA;
      glBegin(GL_TRIANGLES);
      double x, y, z;

      x = cos(longitude*d_phi)*sin(latitude*d_theta);
      y = sin(longitude*d_phi)*sin(latitude*d_theta);
      z = cos(latitude*d_theta);
      glNormal3f(x, y, z);
      glTexCoord2f(static_cast<float>(longitude)/NR_PHI, static_cast<float>(latitude)/NR_THETA);
      glVertex3f(x, y, z);
      x = cos((longitude+1)*d_phi)*sin(latitude*d_theta);
      y = sin((longitude+1)*d_phi)*sin(latitude*d_theta);
      z = cos(latitude*d_theta);
      glNormal3f(x, y, z);
      glTexCoord2f(d_phi,0);
      glTexCoord2f(static_cast<float>(longitude+1)/NR_PHI, static_cast<float>(latitude)/NR_THETA);
      glVertex3f(x, y, z);
      x = cos((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
      y = sin((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
      z = cos((latitude+1)*d_theta);
      glNormal3f(x, y, z);
      glTexCoord2f(static_cast<float>(longitude+1)/NR_PHI, static_cast<float>(latitude+1)/NR_THETA);
      glVertex3f(x, y, z);

      x = cos(longitude*d_phi)*sin(latitude*d_theta);
      y = sin(longitude*d_phi)*sin(latitude*d_theta);
      z = cos(latitude*d_theta);
      glNormal3f(x, y, z);
      glTexCoord2f(static_cast<float>(longitude)/NR_PHI, static_cast<float>(latitude)/NR_THETA);
      glVertex3f(x, y, z);
      x = cos((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
      y = sin((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
      z = cos((latitude+1)*d_theta);
      glNormal3f(x, y, z);
      glTexCoord2f(static_cast<float>(longitude+1)/NR_PHI, static_cast<float>(latitude+1)/NR_THETA);
      glVertex3f(x, y, z);
      x = cos((longitude)*d_phi)*sin((latitude+1)*d_theta);
      y = sin((longitude)*d_phi)*sin((latitude+1)*d_theta);
      z = cos((latitude+1)*d_theta);
      glNormal3f(x, y, z);
      glTexCoord2f(static_cast<float>(longitude)/NR_PHI, static_cast<float>(latitude+1)/NR_THETA);
      glVertex3f(x, y, z);

      glEnd();
    }
  }
  glDisable(GL_TEXTURE_2D);
}

// fish of different colours
void LighthouseWidget::fish() {
  // fixes the normals after scaling
  glEnable(GL_RESCALE_NORMAL);
  glRotatef(45, 1, 0, 0);
  glScalef(0.5,0.5,0.5);

  // render fish according to the colour chosen
  if (redFish) {
    glBegin(GL_POLYGON);
      glColor4f( 1.0, 0.0, 0.0, 1);   // red
      glVertex3f( 0.0, 0.0, 1.0);
      glColor4f(1.0, 0.5, 0.0, 1);    // orange
      glVertex3f( 0.0,-0.5,-0.5);
      glVertex3f( 0.0, 0.0,-1.0);
      glColor4f(1.0, 0.0, 0.0, 1);    // red
      glVertex3f( 0.0, 0.5,-0.5);
    glEnd();
    glBegin(GL_POLYGON);
      glColor4f(1.0, 0.0, 0.0, 1);    // red
      glVertex3f( 0.0, 0.0, 1.0);
      glColor4f(1.0, 1.0, 0.0, 1);    // yellow
      glVertex3f( 0.0, 0.5, 1.5);
      glVertex3f( 0.0,-0.5, 1.5);
    glEnd();
  } else if (yellowFish) {
    glBegin(GL_POLYGON);
      glColor4f( 0.0, 1.0, 0.0, 1);   // green
      glVertex3f( 0.0, 0.0, 1.0);
      glColor4f(1.0, 1.0, 0.0, 1);    // yellow
      glVertex3f( 0.0,-0.5,-0.5);
      glVertex3f( 0.0, 0.0,-1.0);
      glColor4f(0.0, 1.0, 0.0, 1);    // green
      glVertex3f( 0.0, 0.5,-0.5);
    glEnd();
    glBegin(GL_POLYGON);
      glColor4f(1.0, 0.5, 0.0, 1);    // orange
      glVertex3f( 0.0, 0.0, 1.0);
      glColor4f(1.0, 1.0, 0.0, 1);    // yellow
      glVertex3f( 0.0, 0.5, 1.5);
      glVertex3f( 0.0,-0.5, 1.5);
    glEnd();
  } else if (blueFish) {
    glBegin(GL_POLYGON);
      glColor4f(0.0, 1.0, 1.0, 1);    // cyan
      glVertex3f( 0.0, 0.0, 1.0);
      glColor4f( 1.0, 0.0, 1.0, 1);   // magenta
      glVertex3f( 0.0,-0.5,-0.5);
      glVertex3f( 0.0, 0.0,-1.0);
      glColor4f(0.0, 1.0, 1.0, 1);    // cyan
      glVertex3f( 0.0, 0.5,-0.5);
    glEnd();
    glBegin(GL_POLYGON);
      glColor4f(0.5, 0.0, 1.0, 1);    // violet
      glVertex3f( 0.0, 0.0, 1.0);
      glColor4f(0.0, 1.0, 1.0, 1);    // cyan
      glVertex3f( 0.0, 0.5, 1.5);
      glVertex3f( 0.0,-0.5, 1.5);
    glEnd();
  } else if (rgbFish) {
    glBegin(GL_POLYGON);
      glColor4f( 1.0, 0.0, 0.0, 1);   // red
      glVertex3f( 0.0, 0.0, 1.0);
      glColor4f(0.0, 0.0, 1.0, 1);    // blue
      glVertex3f( 0.0,-0.5,-0.5);
      glColor4f(0.0, 1.0, 0.0, 1);    // green
      glVertex3f( 0.0, 0.0,-1.0);
      glColor4f( 1.0, 0.0, 0.0, 1);   // red
      glVertex3f( 0.0, 0.5,-0.5);
    glEnd();
    glBegin(GL_POLYGON);
      glColor4f(1.0, 0.0, 0.0, 1);    // red
      glVertex3f( 0.0, 0.0, 1.0);
      glColor4f(0.0, 1.0, 0.0, 1);    // green
      glVertex3f( 0.0, 0.5, 1.5);
      glColor4f(0.0, 0.0, 1.0, 1);    // blue
      glVertex3f( 0.0,-0.5, 1.5);
    glEnd();
  } else if (cmyFish) {
    glBegin(GL_POLYGON);
      glColor4f( 1.0, 1.0, 0.0, 1);   // yellow
      glVertex3f( 0.0, 0.0, 1.0);
      glColor4f(1.0, 0.0, 1.0, 1);    // magenta
      glVertex3f( 0.0,-0.5,-0.5);
      glColor4f(0.0, 1.0, 1.0, 1);    // cyan
      glVertex3f( 0.0, 0.0,-1.0);
      glVertex3f( 0.0, 0.5,-0.5);
    glEnd();
    glBegin(GL_POLYGON);
      glColor4f(1.0, 1.0, 0.0, 1);    // yellow
      glVertex3f( 0.0, 0.0, 1.0);
      glColor4f(1.0, 0.0, 1.0, 1);    // magenta
      glVertex3f( 0.0, 0.5, 1.5);
      glColor4f(0.0, 1.0, 1.0, 1);    // cyan
      glVertex3f( 0.0,-0.5, 1.5);
    glEnd();
  }
  glDisable(GL_RESCALE_NORMAL);
}

// create lots of jumping fish
void LighthouseWidget::schoolOfFish() {
  glEnable(GL_COLOR_MATERIAL);
  glPushMatrix();
    glTranslatef(0.,-3.,-1.);
    glRotatef((double)-angle*2.0,1.,0.,0.);   // rotate out of sea level and back in
    glTranslatef(14.,3.,2.);
    this->fish();
  glPopMatrix();
  glPushMatrix();
    glTranslatef(-4.,-3.,3.);
    glRotatef((double)-angle*2.1,1.,0.,0.);   // slightly different rotation angles for each fish
    glTranslatef(14.,3.,2.);
    this->fish();
  glPopMatrix();
  glPushMatrix();
    glTranslatef(1.,-3.,6.);
    glRotatef((double)-angle*2.2,1.,0.,0.);
    glTranslatef(14.,3.,2.);
    this->fish();
  glPopMatrix();
  glPushMatrix();
    glTranslatef(-1.,-3.,-4.);
    glRotatef((double)-angle*2.3,1.,0.,0.);
    glTranslatef(14.,3.,2.);
    this->fish();
  glPopMatrix();
  glPushMatrix();
    glTranslatef(5.,-2.3,0.);
    glRotatef((double)-angle*2.4,1.,0.,0.);
    glTranslatef(14.,3.,2.);
    this->fish();
  glPopMatrix();
  glPushMatrix();
    glTranslatef(8.,-2.3,4.);
    glRotatef((double)-angle*2.5,1.,0.,0.);
    glTranslatef(14.,3.,2.);
    this->fish();
  glPopMatrix();
  glPushMatrix();
    glTranslatef(6.,-2.3,-5.);
    glRotatef((double)-angle*2.6,1.,0.,0.);
    glTranslatef(14.,3.,2.);
    this->fish();
  glPopMatrix();
  glPushMatrix();
    glTranslatef(7.,-2.3,7.);
    glRotatef((double)-angle*2.7,1.,0.,0.);
    glTranslatef(14.,3.,2.);
    this->fish();
  glPopMatrix();

  glColor4f(1,1,1,1);
  glDisable(GL_COLOR_MATERIAL);
}

// called every time the widget needs painting
void LighthouseWidget::paintGL()
  { // paintGL()
	// clear the widget
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glShadeModel(GL_SMOOTH);

	glMatrixMode(GL_MODELVIEW);
 	glEnable(GL_DEPTH_TEST);

  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glEnable(GL_COLOR_MATERIAL);
  glPushMatrix();
  this->sea();
  glPopMatrix();
  glDisable(GL_COLOR_MATERIAL);

  glPushMatrix();
  this->island();
  this->boat();
  glPopMatrix();

  glPushMatrix();
  this->schoolOfFish();
  glPopMatrix();

  glPushMatrix();
    glTranslatef(0.,-3.,0.);
    // spin
    glRotatef((double)angle/5,1.,0.,0.);
    glTranslatef(-11.,3.,-8.);
    glScalef(1, -1, 1);
    glRotatef(180, 0, 1, 0);
    glRotatef(-90, 1, 0, 0);
    this->earth();
  glPopMatrix();

	glLoadIdentity();
  gluLookAt(-viewAngle/10.,0.5,0., 0.0,-0.2,0.0, 0.0,1.0,0.0);

	// flush to screen
	glFlush();

	} // paintGL()

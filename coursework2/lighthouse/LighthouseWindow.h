#ifndef __GL_POLYGON_WINDOW_H__
#define __GL_POLYGON_WINDOW_H__ 1

#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QBoxLayout>
#include <QTimer>
#include <QRadioButton>
#include "LighthouseWidget.h"

class LighthouseWindow: public QWidget
	{
	public:

	// constructor / destructor
	LighthouseWindow(QWidget *parent);
	~LighthouseWindow();

	// visual hierarchy
	// menu bar
	QMenuBar *menuBar;
		// file menu
		QMenu *fileMenu;
			// quit action
			QAction *actionQuit;

	// window layout
	QBoxLayout *windowLayout;

	// beneath that, the main widget
	LighthouseWidget *lighthouseWidget;

	// slider for the speed of the boat
	QSlider *speedSlider;
	// slider for the camera view
	QSlider *viewSlider;

	// timer for animation
	QTimer* ptimer;

	// radiobuttons for fish colours
	QRadioButton* fishButton;
	QRadioButton* fishButton2;
	QRadioButton* fishButton3;
	QRadioButton* fishButton4;
	QRadioButton* fishButton5;

	// resets all the interface elements
	void ResetInterface();
	};

#endif

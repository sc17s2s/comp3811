#include <QApplication>
#include <QVBoxLayout>
#include "LighthouseWindow.h"

int main(int argc, char *argv[])
	{ // main()
	// create the application
	QApplication app(argc, argv);

	// create a master widget
  LighthouseWindow *window = new LighthouseWindow(NULL);

	// resize the window
	window->resize(800, 900);

	// show the label
	window->show();

	// start it running
	app.exec();

	// clean up
	delete window;

	// return to caller
	return 0;
	} // main()
